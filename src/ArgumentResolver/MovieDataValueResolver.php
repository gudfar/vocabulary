<?php declare(strict_types = 1);

namespace App\ArgumentResolver;

use App\Request\MovieDataRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * Class MovieDataValueResolver
 * @package App\ArgumentResolver
 */
class MovieDataValueResolver implements ArgumentValueResolverInterface
{
    /**
     * @var Session
     */
    private $session;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * MovieDataValueResolver constructor.
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(SessionInterface $session, EntityManagerInterface $entityManager)
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return bool
     */
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return MovieDataRequest::class === $argument->getType();
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return \Generator
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $movieDataRequest = new MovieDataRequest();

        if ($movieData = $this->session->get('movie_data')) {
            //https://stackoverflow.com/questions/7473872/entities-passed-to-the-choice-field-must-be-managed/7931437
            $movieDataRequest->setMovie($this->entityManager->merge($movieData['movie']));

            $movieDataRequest->setSeason($movieData['season']);
            $movieDataRequest->setEpisode($movieData['episode']);

        }

        yield $movieDataRequest;
    }

}