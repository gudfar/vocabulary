<?php declare(strict_types = 1);

namespace App\ArgumentResolver;

use App\Form\SearchPageType;
use App\Request\SearchRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * Class MovieDataValueResolver
 * @package App\ArgumentResolver
 */
class SearchValueResolver implements ArgumentValueResolverInterface
{
    /**
     * @var Session
     */
    private $session;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var SearchPageType
     */
    private $searchPageType;

    /**
     * SearchValueResolver constructor.
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param SearchPageType $searchPageType
     */
    public function __construct(SessionInterface $session, EntityManagerInterface $entityManager, SearchPageType $searchPageType)
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->searchPageType = $searchPageType;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return bool
     */
    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return SearchRequest::class === $argument->getType();
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return \Generator
     */
    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        $searchRequest = new SearchRequest();
        $queryParams = $request->query->all();
        $formPrefix = $this->searchPageType->getBlockPrefix();
        $keys = $queryParams[$formPrefix] ?? null;

        $movieData = $this->session->get('movie_data');
        if ($movieData && !$keys) {
            $searchRequest->setMovie($this->entityManager->merge($movieData['movie']));
            $searchRequest->setSeason($movieData['season']);
            $searchRequest->setEpisode($movieData['episode']);
        }

        yield $searchRequest;
    }

}