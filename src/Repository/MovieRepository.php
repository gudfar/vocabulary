<?php declare(strict_types = 1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class MovieRepository
 * @package App\Repository
 */
class MovieRepository extends EntityRepository
{

}