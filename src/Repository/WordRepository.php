<?php declare(strict_types = 1);

namespace App\Repository;

use App\Request\SearchRequest;
use PDO;
use Doctrine\ORM\EntityRepository;

/**
 * Class WordRepository
 * @package App\Entity
 */
class WordRepository extends EntityRepository
{
    /**
     * @param string $name
     * @return mixed
     */
    public function findByName(string $name)
    {
        return $this->createQueryBuilder('w')
            ->where('w.name LIKE :name')
            ->setParameter('name', '%'.$name.'%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int|null $limit
     * @return array
     */
    public function findLastWords(?int $limit = 10): array
    {
        /**
         * @param $field
         * @return string
         */
        $subQuery = function ($field): string {
            return "select $field from word order by id desc LIMIT 1";
        };

        $sql = 'select 
            w.name as word,
            w.translation as translation,
            m.name as movie,
            w.season as season,
            w.episode as episode
        from word as w LEFT JOIN movie as m ON w.movie_id = m.id
        WHERE w.movie_id = ('.$subQuery('movie_id').')
        AND w.season = ('.$subQuery('season').')    
        AND w.episode = ('.$subQuery('episode').')  
        ORDER BY RAND() LIMIT :limit';

        try {
            $stmt = $this->_em->getConnection()->prepare($sql);
            $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (\Exception $exception) {

        }

        return [];
    }


    /**
     * @param int|null $limit
     * @return array
     */
    public function findLastRandomWords(?int $limit = 10): array
    {
        return $this->createQueryBuilder('w')
            ->select([
                'w.name as word',
                'w.translation as translation',
                'm.name as movie',
                'w.season as season',
                'w.episode as episode',
            ])
            ->leftJoin('w.movie', 'm')
            ->orderBy('RAND()')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param SearchRequest $searchRequest
     * @return mixed
     */
    public function findBySearchCriteria(SearchRequest $searchRequest): array
    {
        $queryBuilder = $this->createQueryBuilder('w')
            ->select([
                'w.name as name',
                'w.translation as translation',
                'w.season as season',
                'w.episode as episode',
            ])
        ;

        if (!empty($searchRequest->getName())) {
            $queryBuilder
                ->where('w.name LIKE :name')
                ->setParameter('name', '%'.$searchRequest->getName().'%');
        }

        if (!empty($searchRequest->getTranslation())) {
            $queryBuilder
                ->andWhere('w.translation LIKE :translation')
                ->setParameter('translation', '%'.$searchRequest->getTranslation().'%');
        }

        if (!empty($searchRequest->getMovie())) {
            $queryBuilder->addSelect('m.name as movie');
            $queryBuilder
                ->leftJoin('w.movie', 'm')
                ->andWhere('m.id = :id')
                ->setParameter('id', $searchRequest->getMovie()->getId());
        }

        if (!empty($searchRequest->getSeason())) {
            $queryBuilder
                ->andWhere('w.season = :season')
                ->setParameter('season', $searchRequest->getSeason());
        }

        if (!empty($searchRequest->getEpisode())) {
            $queryBuilder
                ->andWhere('w.episode = :episode')
                ->setParameter('episode', $searchRequest->getEpisode());
        }

        return $queryBuilder->getQuery()->getResult();
    }
}