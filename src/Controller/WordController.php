<?php declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Word;
use App\Form\SearchPageType;
use App\Form\WordCreateType;
use App\Form\WordSearchType;
use App\Repository\WordRepository;
use App\Request\SearchRequest;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\WordService;

/**
 * Class WordController
 * @package App\Controller
 */
class WordController extends AbstractController
{
    /**
     * @Route("/", methods={"GET", "POST"}, name="words_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actionIndex(Request $request)
    {
        $matchedWords = [];
        $form = $this->createForm(WordSearchType::class);
        $form->handleRequest($request);
        $word = $request->get($form->getName())['name'];
        /**
         * @var $wordRepository WordRepository
         */
        $wordRepository = $this->getDoctrine()->getRepository(Word::class);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $errors = $this->getErrorsFromForm($form);
                if (isset($errors['name'])) {
                    $matchedWords = $wordRepository->findByName($word);
                }
            } else {
                return $this->redirectToRoute('word_create', ['word' => $word]);
            }
        }

        return $this->render('main/index.html.twig', [
            'form'         => $form->createView(),
            'matchedWords' => $matchedWords,
            'lastWords' => $wordRepository->findLastWords()
        ]);
    }


    /**
     * @Route("/create", methods={"GET", "POST"}, name="word_create")
     * @param Request $request
     * @param WordService $wordService\
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actionCreate(Request $request, WordService $wordService)
    {
        $form = $this->createForm(WordCreateType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $wordService->save($form->getData());

            $this->addFlash("success", "Successfully added!");
            return $this->redirectToRoute('words_index');
        }

        return $this->render('main/create.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/search", methods={"GET", "POST"}, name="word_search")
     * @param Request $request
     * @param SearchRequest $searchRequest
     * @param WordService $wordService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actionSearch(Request $request, SearchRequest $searchRequest, WordService $wordService)
    {
        $form = $this->createForm(SearchPageType::class, $searchRequest);
        $form->handleRequest($request);

        $words = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $words = $wordService->search($searchRequest);
        }

        return $this->render('main/search.html.twig', [
            'form'  => $form->createView(),
            'words' => $words
        ]);
    }
    
    /**
     * @param FormInterface $form
     * @return array
     */
    private function getErrorsFromForm(FormInterface $form): array
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }
        return $errors;
    }

}