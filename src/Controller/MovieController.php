<?php declare(strict_types = 1);

namespace App\Controller;

use App\Form\MovieDataType;
use App\Request\MovieDataRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/movie")
 * Class MovieController
 * @package App\Controller
 */
class MovieController extends AbstractController
{

    /**
     * @Route("/save-data", methods={"GET", "POST"}, name="movie_save_data")
     * @param Request $request
     * @param MovieDataRequest $movieDataRequest
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function actionSaveData(Request $request, MovieDataRequest $movieDataRequest, SessionInterface $session)
    {

        $form = $this->createForm(MovieDataType::class, $movieDataRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session->set('movie_data', [
                'movie' => $movieDataRequest->getMovie(),
                'season' => $movieDataRequest->getSeason(),
                'episode' => $movieDataRequest->getEpisode(),
            ]);
            return $this->redirectToRoute('words_index');
        }

        return $this->render('form/_movie_data_form.html.twig', [
            'form' => $form->createView()
        ]);

    }

}