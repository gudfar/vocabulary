<?php declare(strict_types = 1);

namespace App\Request;

use App\Entity\Movie;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class MovieDataRequest
 * @package App\Request
 */
class SearchRequest
{
    /**
     * @var string
     * @Assert\Length(min="1", max="50")
     */
    private $name;
    /**
     * @var string
     * @Assert\Length(min="1", max="50")
     */
    private $translation;

    /**
     * @var Movie
     */
    private $movie;
    /**
     * @var integer
     */
    private $season;
    /**
     * @var integer
     */
    private $episode;

    /**
     * @return Movie|null
     */
    public function getMovie(): ?Movie
    {
        return $this->movie;
    }

    /**
     * @param Movie $movie
     */
    public function setMovie(Movie $movie): void
    {
        $this->movie = $movie;
    }

    /**
     * @return int|null
     */
    public function getSeason(): ?int
    {
        return $this->season;
    }

    /**
     * @param int $season
     */
    public function setSeason(int $season): void
    {
        $this->season = $season;
    }

    /**
     * @return int|null
     */
    public function getEpisode(): ?int
    {
        return $this->episode;
    }

    /**
     * @param int $episode
     */
    public function setEpisode(int $episode): void
    {
        $this->episode = $episode;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTranslation(): ?string
    {
        return $this->translation;
    }

    /**
     * @param string $translation
     */
    public function setTranslation(string $translation): void
    {
        $this->translation = $translation;
    }
}