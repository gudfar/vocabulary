<?php declare(strict_types = 1);

namespace App\Request;
use App\Entity\Movie;

/**
 * Class MovieDataRequest
 * @package App\Request
 */
class MovieDataRequest
{
    /**
     * @var Movie
     */
    private $movie;
    /**
     * @var integer
     */
    private $season;
    /**
     * @var integer
     */
    private $episode;

    /**
     * @return Movie|null
     */
    public function getMovie(): ?Movie
    {
        return $this->movie;
    }

    /**
     * @param Movie $movie
     */
    public function setMovie(Movie $movie): void
    {
        $this->movie = $movie;
    }

    /**
     * @return int|null
     */
    public function getSeason(): ?int
    {
        return $this->season;
    }

    /**
     * @param int $season
     */
    public function setSeason(int $season): void
    {
        $this->season = $season;
    }

    /**
     * @return int|null
     */
    public function getEpisode(): ?int
    {
        return $this->episode;
    }

    /**
     * @param int $episode
     */
    public function setEpisode(int $episode): void
    {
        $this->episode = $episode;
    }
}