<?php declare(strict_types = 1);

namespace App\Service;

use App\Entity\Word;
use App\Repository\WordRepository;
use App\Request\SearchRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class WordService
 * @package App\Service
 */
class WordService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * WordService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    /**
     * @param Word $word
     */
    public function save(Word $word): void
    {
        if ($movieData = $this->session->get('movie_data')) {
            $word->setMovie($movieData['movie']);
            $word->setSeason($movieData['season']);
            $word->setEpisode($movieData['episode']);
        }

        $this->entityManager->merge($word);
        $this->entityManager->flush();
    }

    /**
     * @param SearchRequest $searchRequest
     * @return array
     */
    public function search(SearchRequest $searchRequest): array
    {
        /**
         * @var $repository WordRepository
         */
        $repository = $this->entityManager->getRepository(Word::class);
        return $repository->findBySearchCriteria($searchRequest);
    }

}