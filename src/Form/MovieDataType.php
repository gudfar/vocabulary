<?php declare(strict_types = 1);

namespace App\Form;
use App\Entity\Movie;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class MovieType
 * @package App\Form
 */
class MovieDataType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('movie', EntityType::class, [
                'class' => Movie::class,
                'choice_label' => 'name',
                'label' => false,
                'placeholder' => 'Select your movie',
            ])
            ->add('season', ChoiceType::class, [
                'choices' => array_combine(range(1,100), range(1,100)),
                'label' => false,
                'placeholder' => 'Select season',
            ])->add('episode', ChoiceType::class, [
                'choices' => array_combine(range(1,100), range(1,100)),
                'placeholder' => 'Select episode',
                'label' => false
            ])
        ;
    }

}