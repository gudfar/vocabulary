<?php declare(strict_types = 1);

namespace App\Form;

use App\Entity\Word;
use App\Validator\Constraints\UniqueConstraint;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class WordSearchType
 * @package App\Form
 */
class WordSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 1, 'max' => 50]),
                    new UniqueConstraint([
                        'entityClass' => Word::class,
                        'field' => 'name'
                    ])
                ],
                'label' => false
            ]);
    }
}
