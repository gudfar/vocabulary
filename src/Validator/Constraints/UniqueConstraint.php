<?php declare(strict_types = 1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueConstraint extends Constraint
{
    /**
     * @var string
     */
    public $message = 'This value is already used.';

    /**
     * @var string
     */
    public $entityClass;

    /**
     * @var string
     */
    public $field;


    /**
     * @return array
     */
    public function getRequiredOptions(): array
    {
        return ['entityClass', 'field'];
    }

    /**
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }
}