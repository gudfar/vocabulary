<?php declare(strict_types = 1);

namespace App\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class UniqueConstraintValidator
 * @package App\Validator\Constraints
 */
class UniqueConstraintValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * UniqueValueInEntityValidator constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        /**
         * @var $entityRepository EntityRepository
         * @var $constraint UniqueConstraint
         */
        if (!$constraint instanceof UniqueConstraint) {
            throw new UnexpectedTypeException($constraint, UniqueConstraint::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $entityRepository = $this->entityManager->getRepository($constraint->entityClass);

        $isRecordExist = (bool)$entityRepository->createQueryBuilder('q')
            ->where('q.' . $constraint->field . ' LIKE :value')
            ->setParameter('value', '%'.$value.'%')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();


        if ($isRecordExist) {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}